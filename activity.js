

// INSERT ONE

db.hotel.insertOne({
	name: "single",
	accomodates: 2 ,
	price: 1000 ,
	description: "A simple toom with all the basic necessities",
	rooms_availabe: 10 ,
	isAvailable : false
});


// INSERT MANY

db.hotel.insertMany([
	{
		name: "double",
		accomodates: 3 ,
		price: 2000 ,
		description: "A room fit for a small family going on a vacation",
		rooms_availabe: 5 ,
		isAvailable: false
	},
	{
		name: "queen",
		accomodates: 4 ,
		price: 4000 ,
		description: "A room with a queen sized bed perfect for a simple getaway",
		rooms_availabe: 15 ,
		isAvailable: false
	}

]);



// FIND METHOD

db.hotel.find({name: "double"});


// UPDATE ONE METHOD 

db.hotel.updateOne(
	{name: "queen"},
	{
		$set:{
			rooms_availabe: 0,
		}
	}

);

// deleteMany Method 
db.hotel.deleteMany({
	rooms_availabe: 0 ,
});


